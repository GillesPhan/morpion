import { useEffect, useState } from "react";
import { Spinner } from "react-bootstrap";
import { Unsubscribe } from "redux";
import HttpClientStore from "../../stores/HttpClient.store";
import './Loader.css';

const Loader = () => {

  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    
    const store = HttpClientStore.getStore();
    const unsubscribe: Unsubscribe = store.subscribe(() => setLoading(store.getState().isRequestPending));

    return () => unsubscribe();
  });

  return(
    <div className="loader">
      {
        isLoading
        &&
        <Spinner animation="border" variant="info" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      }
    </div>
  );
}

export default Loader;