import { useEffect, useState } from "react";
import { Unsubscribe } from "redux";
import TimeStore from "../../stores/Time.store";
import {msToHour} from "../../utils/DateUtils";

const Time = () => {

  const [currentTime, setCurrentTime] = useState('...');
  
  useEffect(() => {
    
    const store = TimeStore.getStore();

    const unsubscribe: Unsubscribe = store.subscribe(() => setCurrentTime(msToHour(store.getState().currentTime)));
    return () => unsubscribe();
  });


  return (
    <em>Il est {currentTime}</em>
  );
};

export default Time;