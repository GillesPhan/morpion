import { Card } from "react-bootstrap";
import Time from "./Time.component";


const CardPanel = () => {

  const version = process.env.REACT_APP_VERSION;

  return (
    <Card>
      <Card.Body>
        <Card.Title>Projet test React.</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">Last update: TODO, version: {version}</Card.Subtitle>
        <Card.Text className="text-start">
          - Dépôt <a href="https://bitbucket.org/GillesPhan/morpion/src">Bitbucket</a><br />
          - <a href="https://react-bootstrap.github.io/">React-Bootstrap</a><br />
          - API <a href="https://public.opendatasoft.com/explore/dataset/prix_des_carburants_j_7">OpenDataSoft</a> pour les prix des carburants<br />
          - <Time/>
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

export default CardPanel;