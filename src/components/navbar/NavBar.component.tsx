import { Link, useLocation } from "react-router-dom";

const NavBar = () => {

  const currentLocation: String = useLocation().pathname;
  const isHome = currentLocation === '/home';
  const isProfile = currentLocation === '/profile';
  const isAbout = currentLocation === '/about';
  
  return (
    <header className="p-3 mb-3 border-bottom">
      <div className="container">
        <div className="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
          <Link className="text-dark display-6 text-decoration-none mr-4" to="/home">#FuelStat</Link>
          <ul className="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
            <li>
              <Link className={`nav-link px-2 ${isHome ? 'link-secondary' : 'link-dark'}`} to="/home">Accueil</Link>
            </li>
            <li>
              <Link className={`nav-link px-2 ${isProfile ? 'link-secondary' : 'link-dark'}`} to="/profile">Profile</Link>
            </li>
            <li>
              <Link className={`nav-link px-2 ${isAbout ? 'link-secondary' : 'link-dark'}`} to="/about">À propos</Link>
            </li>
          </ul>
        </div>
      </div>
    </header>
  );
};

export default NavBar;