import { Card, Col, Row } from 'react-bootstrap';
import Data from '../../data/data.type';
import FuelStore from '../../stores/Fuel.store';
import { getSexyDateFromStrDate, getDiffDate } from '../../utils/DateUtils'

type Props = {
  datas: Data[],
};
enum Fuel {
  SP95 = "SP95",
  SP95E10 = "SP95-E10",
  SP98 = "SP98",
  Gazole = "Gazole"
};
enum Vehicle {
  Car = 1,
  Bike = 2,
  Horse = 3
};
const FuelCard = (props: Props) => {

  const store = FuelStore.getStore();
  const vehicle: any = JSON.parse(localStorage.getItem("vehicle") || '{}');

  const {datas} = props;
  const cards: any[] = [];

  if (Object.keys(props.datas).length === 0) {
    return (<div></div>);
  }

  const getFuelData = (fuelLabel: String, fuelValue: number) => {

    if(fuelValue) {
      return (
        <span>
          <span className="text-info fs-5">{fuelLabel} :</span>
          <span className="fs-5 fw-bold">{fuelValue}€</span>
        </span>
      );
    } else {
      return (
        <span>
          <span className="text-danger fw-bold fs-5">{fuelLabel} :</span>
          <span className="fs-5 fw-bold">-</span>
        </span>
      );
    }
  };

  const handleClickStation = (station: Data) => store.dispatch({ type: 'setSelectedStation', value: station });

  datas.forEach((data: Data, id: number) => {
    
    const isMyFuelAvailable: boolean = !!(
      (vehicle.fuelVehicle === 1 && data.prices.sp95) ||
      (vehicle.fuelVehicle === 2 && data.prices.e10) ||
      (vehicle.fuelVehicle === 3 && data.prices.sp98) ||
      (vehicle.fuelVehicle === 4 && data.prices.gazole)
    );

    cards.push(
      <Col key={id} md={12} lg={4}>
        <Card onClick={() => handleClickStation(data)} className={`pointer mb-4 card-station ${isMyFuelAvailable ? '' : 'border-danger'}`}>
          <Card.Body>
              <Card.Title className="text-center display-6">{data.brand || '-'}</Card.Title>
              <Card.Subtitle className="text-center mb-2 text-muted small">
                {data.city} (<em>{data.address}</em>)
              </Card.Subtitle>
              <hr/>
              <Card.Text className="mx-5 pt-2">
                {getFuelData(Fuel.SP95E10, data.prices.e10)} <br/>
                {getFuelData(Fuel.SP95, data.prices.sp95)} <br/>
                {getFuelData(Fuel.SP98, data.prices.sp98)} <br/>
                {getFuelData(Fuel.Gazole, data.prices.gazole)} <br/>
                <br/>
                <span className="text-muted">
                  <small>{getSexyDateFromStrDate(data.date)} (<em>{getDiffDate(data.date)}</em>)</small>
                </span>
              </Card.Text>
            </Card.Body>
        </Card>
      </Col>
    );
  });

  return (
    <div>
      <Row>
        {cards}
      </Row>
    </div>
  );

};

export default FuelCard;