import { faEuro, faGasPump, faHourglass, faMapLocationDot } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { Card, Col, Row } from "react-bootstrap";
import { Unsubscribe } from "redux";
import Data from "../../data/data.type";
import FuelStore from "../../stores/Fuel.store";
import { getDiffDate } from "../../utils/DateUtils";

enum Fuel {
  SP95 = 'SP95',
  SP95E10 = 'SP95-E10',
  SP98 = 'SP98',
  Gazole = 'Gazole',
};

const Statistics = () => {

  const emptyStation: Data = {
    address: '',
    date: '',
    brand: '',
    city: '',
    prices: {
      e10: 0,
      sp95: 0,
      sp98: 0,
      gazole: 0
    },
    location: { x: 0, y:0 }
  };
  const defaultUrl = 'https://www.google.com/maps/';
  const [selectedStation, setSelectedStation] = useState(emptyStation);
  const [priceFullRefill, setPriceFullRefill] = useState(0);
  const [lastUpdateDate, setLastUpdateDate] = useState('-');
  const [fuelType, setFuelType] = useState('-');
  const [urlGoogleMaps, setUrlGoogleMaps] = useState(defaultUrl);
  const [isLocationAvailable, setLocationAvailable] = useState(false);

  const store = FuelStore.getStore();
  useEffect(() => {
    
    const unsubscribe: Unsubscribe = store.subscribe(() => {

      const newSelectedStation = store.getState().selectedStation;
      setSelectedStation(newSelectedStation);
      
      const vehicle: any = JSON.parse(localStorage.getItem("vehicle") || '{}');
      
      setLastUpdateDate(getDiffDate(newSelectedStation.date));
      switch(vehicle.fuelVehicle) {
        case 1:
          setFuelType(Fuel.SP95);
          setPriceFullRefill(vehicle.fuelCapacity * newSelectedStation.prices.sp95);
          break;
        case 2:
          setFuelType(Fuel.SP95E10);
          setPriceFullRefill(vehicle.fuelCapacity * newSelectedStation.prices.e10);
          break;
        case 3:
          setFuelType(Fuel.SP98);
          setPriceFullRefill(vehicle.fuelCapacity * newSelectedStation.prices.sp98);
          break;
        case 4:
          setFuelType(Fuel.Gazole);
          setPriceFullRefill(vehicle.fuelCapacity * newSelectedStation.prices.gazole);
          break;
      }

      if(newSelectedStation.location.x !== 0) {
        setUrlGoogleMaps(defaultUrl + 'search/?api=1&query=' + newSelectedStation.location.x + ',' + newSelectedStation.location.y);
        setLocationAvailable(true);
      } else {
        setUrlGoogleMaps(defaultUrl);
        setLocationAvailable(false);
      }

    });

    return () => unsubscribe();
  });


  /**
   * On click on the Card "Map", redirect to google map if the location of station is available.
   */
  const handleClickCardMap = () => {
    if(isLocationAvailable) {
      window.open(urlGoogleMaps);
    }
  }


  const emptyStatitics = (<div></div>);

  const completedStatitics = (
    <div>
      <hr />
      <Row>
        <Col className="text-center">
          <h2 className="display-6">
            Station sélectionnée : <strong>{selectedStation.brand} (<em>{selectedStation.address}</em>)</strong>
          </h2>
        </Col>
      </Row>

      <hr/>

      <Row className="mb-5">
        <Col xs={3}>
          <Card className="text-center pt-4 pb-4">
            <FontAwesomeIcon icon={faEuro} size="4x" className="pb-3"/>
            {priceFullRefill}€
          </Card>
        </Col>
        <Col xs={3}>
          <Card className="text-center pt-4 pb-4">
            <FontAwesomeIcon icon={faGasPump} size="4x" className="pb-3"/>
            {fuelType}
          </Card>
        </Col>
        <Col xs={3}>
          <Card className="text-center pt-4 pb-4">
            <FontAwesomeIcon icon={faHourglass} size="4x" className="pb-3"/>
            {lastUpdateDate}
          </Card>
        </Col>
        <Col xs={3}>
          <Card className={`text-center pt-4 pb-4 ${isLocationAvailable ? 'pointer text-primary' : 'not-allowed text-danger'}`} onClick={handleClickCardMap}>
            <FontAwesomeIcon icon={faMapLocationDot} size="4x" className="pb-3"/>
            Accéder via Google Map
          </Card>
        </Col>
      </Row>
    </div>
  );

  return ( selectedStation.address === '' ? emptyStatitics : completedStatitics );
};

export default Statistics;