import { useEffect, useState } from "react";
import { Row, Col } from 'react-bootstrap';
import FuelCard from './FuelCards.component';
import About from './About.component';
import Search from './Search.component';
import ShortcutCp from './ShortcutCp.component';
import OpenDataSoftApiService from '../../services/OpenDataSoftApi.service';
import HttpClientStore from '../../stores/HttpClient.store';
import Statistics from "./Statistics.component";
import { Unsubscribe } from "redux";
import FuelService from "../../services/Fuel.service";
import FuelStore from "../../stores/Fuel.store";

const Home = () => {

  const fuelStore = FuelStore.getStore();
  const defaultCpOrStoredCp = fuelStore.getState().selectedCp === "" ? "69490" : fuelStore.getState().selectedCp;
  const [fuel, setFuel] = useState({
    about: {
      nbResults: 0,
      cp: "-"
    },
    datas: []
  });
  const [isLoading, setLoading] = useState(false);
  const [cp, setCp] = useState(defaultCpOrStoredCp);
  const [cpValue, setCpValue] = useState(defaultCpOrStoredCp);
  
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    
    if(cp !== fuelStore.getState().selectedCp) {
      // First time or new request
      OpenDataSoftApiService.getFuelStatsFromCp(cp).then(response => {
        fuelStore.dispatch({ type: 'setResponseData', value: response.data });
        updateDisplay(response.data);
      });
    } else {
      // Recover data from store
      updateDisplay(fuelStore.getState().responseData);
    }
    fuelStore.dispatch({ type: 'setSelectedCp', value: cp });
    
    const httpStore = HttpClientStore.getStore();
    const unsubscribeHttp: Unsubscribe = httpStore.subscribe(() => {
      setLoading(httpStore.getState().isRequestPending);
    });
    return () => unsubscribeHttp();
  }, [cp, fuelStore]);

  /**
   * Update the table of fuels, the "about" and the postcode.
   *
   * @param responseData The data returned by request OR the data stored.
   */
  const updateDisplay = (responseData: any) => {

    const formatedFuel = FuelService.mapData(responseData);
    setFuel(formatedFuel);
    setCp(formatedFuel?.about?.cp);
    setCpValue(formatedFuel?.about?.cp);
  }

  /**
   * Catch the event and update the state of CP.
   *
   * @param {*} e The event.
   */
  const handleCpChange = (e: any) => setCpValue(e.target.value);

  /**
   * Handle click on Search button, and call api to update datas.
   */
  const handleSearchSubmited = () => setCp(cpValue);
  
  /**
   * Catch the event and update the state of CP, and call the API.
   *
   * @param {*} e The event.
   */
  const handleShortcutSelected = (e: any) => setCp(e.target.value);

  return (
    <div>
      <Row className="mt-4">
        <Col xs={8} md={3}>
          <Search cp={cpValue} onCpChange={handleCpChange} onSubmit={handleSearchSubmited} isLoading={isLoading}/>
        </Col>
        <Col xs={4} md={3}>
          <ShortcutCp onCitySelected={handleShortcutSelected} isLoading={isLoading}/>
        </Col>
        <Col xs={12} md={6}>
          <About nbResults={fuel?.about?.nbResults} cp={fuel?.about?.cp} />
        </Col>
      </Row> 
     
      <Row>
        <Col>
          <FuelCard datas={fuel?.datas || {}} />
        </Col>
      </Row>

      <Row>
        <Col>
          <Statistics />
        </Col>
      </Row>
    </div>
  );
}
export default Home;
