type Props = {
  nbResults: number,
  cp: string
};

const About = (props: Props) => {
  const { nbResults, cp } = props;

  return (
    <div>
      <em>{nbResults || '...'} </em> station{nbResults > 1 ? 's' : ''}&nbsp;
      trouvée{nbResults > 1 ? 's' : ''} avec le code postal "<em>{cp || '...'}</em>"
    </div>
  );
};

export default About;