import { Button, ButtonGroup } from "react-bootstrap";

type Props = {
  isLoading: boolean,
  onCitySelected: any
};
type Location = {
  key?: number,
  name: string,
  cp: string,
  color: string
};

const ShortcutCp = (props: Props) => {

  const {isLoading} = props;
  const locations: Location[] = [
    {key: 1 ,name: "Tarare", cp: "69490", color: "outline-primary"},
    {key: 2 ,name: "Le Coteau", cp: "42120", color: "outline-danger"},
    {key: 3 ,name: "Ecully", cp: "69130", color: "outline-danger"},
    {key: 4 ,name: "Lyon 9", cp: "69009", color: "outline-danger"},
  ];

  const buttons: any[] = [];
  locations.forEach((element) => {
    buttons.push(<Button key={element.key} onClick={props.onCitySelected} value={element.cp} variant={element.color} size="sm" disabled={isLoading}>{element.name}</Button>);
  });

  return (
    <ButtonGroup>{buttons}</ButtonGroup>
  );
}

export default ShortcutCp;