import React from "react";

type Props = {
  isLoading: boolean,
  onSubmit: any,
  cp: string,
  onCpChange: React.ChangeEventHandler<HTMLInputElement>,
};

const Search = (props: Props) => {
  const {isLoading} = props;

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    props.onSubmit();
  };

  return (
    <form className="input-group" onSubmit={onSubmit}>
      <input type="number" className="form-control mb-3" name="CP" placeholder="CP" value={props.cp} onChange={props.onCpChange} disabled={isLoading}/>
      <button type="button" className="btn btn-outline-secondary mb-3" onClick={props.onSubmit} disabled={isLoading}>Search</button>
    </form>
  );
};

export default Search;