import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCar, faPersonBiking, faHorse } from '@fortawesome/free-solid-svg-icons'

import { Button, ButtonGroup, Col, FormControl, InputGroup, Row, ToggleButton } from "react-bootstrap"
import { useEffect, useState } from "react";

enum Vehicle {
  Car = 1,
  Bike = 2,
  Horse = 3
};
enum Fuel {
  SP95 = 1,
  SP95E10 = 2,
  SP98 = 3,
  Gazole = 4
};

const Profile = () => {

  // TODO : extraire la logique du localstorage dans un fichier (voir https://blog.logrocket.com/using-localstorage-react-hooks/)
  // TODO : ajouter le péage
  const [vehicleTypeValue, setVehicleTypeValue] = useState(() => {
    const saved = localStorage.getItem("vehicle");
    return saved ? JSON.parse(saved).vehicleType : Vehicle.Car;
  });
  const [fuelCapacityValue, setFuelCapacityValue] = useState(() => {
    const saved = localStorage.getItem("vehicle");
    return saved ? JSON.parse(saved).fuelCapacity : 0;
  });
  const [averageConsommationValue, setAverageConsommationValue] = useState(() => {
    const saved = localStorage.getItem("vehicle");
    return saved ? JSON.parse(saved).averageConsommation : 0;
  });
  const [fuelVehicleValue, setFuelValue] = useState(() => {
    const saved = localStorage.getItem("vehicle");
    return saved ? JSON.parse(saved).fuelVehicle : -1;
  });
  const [distanceValue, setDistanceValue] = useState(() => {
    const saved = localStorage.getItem("vehicle");
    return saved ? JSON.parse(saved).distance : 0;
  });
  const [nbTripsValue, setNbTripsValue] = useState(() => {
    const saved = localStorage.getItem("vehicle");
    return saved ? JSON.parse(saved).nbTrips : 0;
  });

  useEffect(() => {

    localStorage.setItem("vehicle",JSON.stringify({
      vehicleType: vehicleTypeValue,
      fuelCapacity: fuelCapacityValue,
      averageConsommation: averageConsommationValue,
      fuelVehicle: fuelVehicleValue,
      distance: distanceValue,
      nbTrips: nbTripsValue
    }))
  }, [vehicleTypeValue, fuelCapacityValue, averageConsommationValue, fuelVehicleValue, distanceValue, nbTripsValue]);

  const vehiclesTypes = [
    { icon:faCar, value: Vehicle.Car },
    { icon:faPersonBiking, value: Vehicle.Bike },
    { icon:faHorse, value: Vehicle.Horse },
  ];
  const fuels = [
    { label:'SP95', value: Fuel.SP95 },
    { label:'SP95-E10', value: Fuel.SP95E10 },
    { label:'SP98', value: Fuel.SP98 },
    { label:'Gazole', value: Fuel.Gazole },
  ];

  /**
   * Save all data locally in the device.
   */
  const save = () => {

    console.log('pouh');
    // save
    // display toaster ? 
  }

  return (
    <div>
      <h2 className="display-6">Quel est le type de véhicule ?</h2>
      <Row className="mb-3">
        <Col xs lg={4}>
          <ButtonGroup>
            {vehiclesTypes.map((vehicleType, idx) => (
              <ToggleButton
                key={idx}
                id={`vehicle-type-${idx}`}
                type="radio"
                variant="outline-secondary"
                name="vehicleType"
                value={vehicleType.value}
                checked={vehicleTypeValue === vehicleType.value}
                size="lg"
                onChange={(e) => setVehicleTypeValue(parseInt(e.currentTarget.value))}>
                <FontAwesomeIcon icon={vehicleType.icon} />
              </ToggleButton>
            ))}
          </ButtonGroup>
        </Col>
      </Row>

      <h2 className="display-6">Quel est la capacité du réservoire ?</h2>
      <Row className="mb-3">
        <Col xs lg={4}>
          <InputGroup>
            <FormControl
              placeholder="-"
              aria-label="-"
              aria-describedby="fuelCapacity"
              type="number"
              min={0}
              value={fuelCapacityValue}
              onChange={(e) => setFuelCapacityValue(parseInt(e.currentTarget.value))}
            />
            <InputGroup.Text id="fuelCapacity">Litres</InputGroup.Text>
          </InputGroup>
        </Col>
      </Row>

      <h2 className="display-6">Quel est la consommation moyenne</h2>
      <Row className="mb-3">
        <Col xs lg={4}>
          <InputGroup>
            <FormControl
              placeholder="-"
              aria-label="-"
              aria-describedby="averageConsommation"
              type="number"
              min={0}
              step={0.1}
              value={averageConsommationValue}
              onChange={(e) => setAverageConsommationValue(parseFloat(e.currentTarget.value))}
            />
            <InputGroup.Text id="averageConsommation">L / 100km</InputGroup.Text>
          </InputGroup>
        </Col>
      </Row>

      <h2 className="display-6">Quel est le(<em>s</em>) carburant(<em>s</em>) utilisé(<em>s</em>) ?</h2>
      <Row className="mb-3">
        <Col xs lg={4}>
          <ButtonGroup>
            {fuels.map((fuel, idx) => (
              <ToggleButton
                key={idx}
                id={`fuel-${idx}`}
                type="radio"
                variant="outline-secondary"
                name="fuel"
                value={fuel.value}
                checked={fuelVehicleValue === fuel.value}
                size="lg"
                onChange={(e) => setFuelValue(parseInt(e.currentTarget.value))}>
                {fuel.label}
              </ToggleButton>
            ))}
          </ButtonGroup>
        </Col>
      </Row>

      <h2 className="display-6">Distance maison - travail (<em>aller simple</em>) ?</h2>
      <Row className="mb-5">
        <Col xs lg={4}>
          <InputGroup>
            <FormControl
              placeholder="-"
              aria-label="-"
              aria-describedby="distance"
              type="number"
              min={0}
              step={0.1}
              value={distanceValue}
              onChange={(e) => setDistanceValue(parseFloat(e.currentTarget.value))}
            />
            <InputGroup.Text id="distance">km</InputGroup.Text>
          </InputGroup>
        </Col>
      </Row>

      <h2 className="display-6">Nombre de trajets hebdomadaire ?</h2>
      <Row className="mb-5">
        <Col xs lg={4}>
          <InputGroup>
            <FormControl
              placeholder="-"
              aria-label="-"
              aria-describedby="nbTrips"
              type="number"
              min={0}
              max={5}
              value={nbTripsValue}
              onChange={(e) => setNbTripsValue(parseInt(e.currentTarget.value))}
            />
            <InputGroup.Text id="nbTrips">aller/retour</InputGroup.Text>
          </InputGroup>
        </Col>
      </Row>

      <Row className="mb-3">
        <Col>
          <Button variant="outline-success" size="lg" onClick={save} disabled>Sauvegarder</Button>
        </Col>
      </Row>
    </div>
  );
};

export default Profile;