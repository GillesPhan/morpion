/**
 * Return a formated date from a String.
 *
 * @param {string} strDate The date to format.
 * @returns A String with formated date.
 */
const getSexyDateFromStrDate = (strDate: string) => {
  let res = '';

  const date = new Date(getDateFromString(strDate));
  res += labelDay[date.getDay()]
  res += ' ' + date.getDate();
  res += ' ' + labelMonth[date.getMonth()];
  res += ' ' + date.getFullYear();
  
  return res;
};

/**
 * Get the difference between a date and now (in ms).
 *
 * @param {string} strDate The string Date to compare.
 * @returns The difference in ms.
 */
const getDiffDate = (strDate: string) => {

  return msToTime(Date.now() - getDateFromString(strDate));
};

/**
 * Return a formated date from ms.
 *
 * @param {number} s Time in ms.
 * @returns A string.
 */
const msToTime = (s: number) => {

  s = (s>0) ? s : 0;
  s /= 1000;
  const secs = Math.floor(s % 60);
  s = (s - secs) / 60;
  const mins = Math.floor(s % 60);
  let hrs = Math.floor((s - mins) / 60);
  const day = Math.floor(hrs/24);
  hrs = hrs % 24;

  return day + ' jours, ' + hrs + 'h' + mins + 'min';
};

/**
 * Return a formated hour from ms.
 *
 * @param {number} s Time in ms.
 * @returns A string.
 */
 const msToHour = (s: number) => {

  s = (s>0) ? s : 0;
  s /= 1000;
  const secs = Math.floor(s % 60);
  s = (s - secs) / 60;
  const mins = Math.floor(s % 60);
  let hrs = Math.floor((s - mins) / 60);
  hrs = hrs % 24;

  return hrs + 'h' + mins + 'min' + secs;
};


export { getSexyDateFromStrDate, getDiffDate, msToTime, msToHour };

// -------------------------------------- //

/**
 * Get a Date object from String.
 *
 * @param {string} strDate Date au format "YYYY-MM-DDTHH:MM:SS+00:00". Exemple : "2022-01-07T10:51:02+00:00"
 * @returns Un objet Date.
 */
function getDateFromString(strDate: string) {

  // TODO cas aux limites
  return Date.parse(strDate);
};

const labelDay = [
  'Dimanche',
  'Lundi',
  'Mardi',
  'Mercredi',
  'Jeudi',
  'Vendredi',
  'Samedi',
];
const labelMonth = [
  'Décembre',
  'Janvier',
  'Février',
  'Mars',
  'Avril',
  'Mai',
  'Juin',
  'Juillet',
  'Août',
  'Septembre',
  'Octobre',
  'Novembre'
];