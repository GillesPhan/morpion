import { msToTime } from './DateUtils';

describe('msToTime', () => {

  it('function should exist', () => {
    
    expect(msToTime).toBeDefined;
  });

  it('59999ms equal 0min', () => {
  
    const timeToConvert: number = 59999;
    const expectedResult: string = '0 jours, 0h0min';
    const result = msToTime(timeToConvert);
    expect(result).toEqual(expectedResult);
  });

  it('0ms equal 0min', () => {
  
    const timeToConvert: number = 0;
    const expectedResult: string = '0 jours, 0h0min';
    const result = msToTime(timeToConvert);
    expect(result).toEqual(expectedResult);
  });

  it('60000ms equal 1min', () => {
  
    const timeToConvert: number = 60000;
    const expectedResult: string = '0 jours, 0h1min';
    const result = msToTime(timeToConvert);
    expect(result).toEqual(expectedResult);
  });

  it('60001ms equal 1min', () => {
  
    const timeToConvert: number = 60001;
    const expectedResult: string = '0 jours, 0h1min';
    const result = msToTime(timeToConvert);
    expect(result).toEqual(expectedResult);
  });

  it('negative value should not fail', () => {
  
    const timeToConvert: number = -9999999;
    const expectedResult: string = '0 jours, 0h0min';
    const result = msToTime(timeToConvert);
    expect(result).toEqual(expectedResult);
  });
});
