import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from './components/home/Home.component';
import Profile from "./components/profile/Profile.component";
import About from './components/about/About.component';
import NavBar from './components/navbar/NavBar.component';
import NotFound from './components/not-found/NotFound.component';
import Loader from "./components/loader/Loader.component";

const App = () => {

  return (
    <div className="container">
      <BrowserRouter>
        <Loader/>
        <NavBar/>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="home" element={<Home />} />
          <Route path="profile" element={<Profile />} />
          <Route path="about" element={<About />} />
          <Route path="*" element={<NotFound />} />
        </Routes>          
      </BrowserRouter>
    </div>
  );
}

export default App;
