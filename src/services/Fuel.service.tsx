const FuelService = {
  /**
   * Map response into our object.
   *
   * @returns The data.
   * @param {*} json The response of the call.
   */
  mapData: (json: any) => {

    const result = {
      about: {
        cp: getCp(json),
        availableFuel: getAvailableFuel(json),
        nbResults: getNbResult(json)
      },
      datas: getDatas(json)
    };
    
    return result;
  }
};

/**
 * Get the postal code. If multiple postal code is available, return the first of list.
 *
 * @param {*} json The response of the call.
 * @returns 
 */
const getCp = (json: any) => {

  // TODO manage malformated json
  return (json.facet_groups
    .find((facetName: any) => facetName.name === "cp")
    .facets[0] || {})
    .name
};

/**
 * Get the list of available fuel.
 *
 * @param {*} json The response of the call.
 * @returns 
 */
const getAvailableFuel = (json: any) => {
  
  return json.facet_groups
    .find((facetGroup: any) => facetGroup.name === "shortage")
    .facets
    .map((facet: any) => {
      return {fuel: facet.name, available: facet.available}
    })
};

/**
 * Get the number of station founded.
 *
 * @param {*} json The response of the call.
 * @returns 
 */
const getNbResult = (json: any) => {

  return json.nhits;
};

/**
 * Map data and return a formated list, with prices and informations.
 *
 * @param {*} json  The response of the call.
 * @returns 
 */
  const getDatas = (json: any) => {

    return json?.records
    ?.map((record: any) => {
        return {
          date: record.record_timestamp,
          brand: record.fields.brand,
          address: record.fields.address,
          city: record.fields.city,
          prices: {
            gazole: record.fields.price_gazole,
            e10: record.fields.price_e10,
            sp95: record.fields.price_sp95,
            sp98: record.fields.price_sp98
          },
          location: {
            x: record.fields.geo_point ? record.fields.geo_point[0] : 0,
            y: record.fields.geo_point ? record.fields.geo_point[1] : 0
          }
        };
      });
};

export default FuelService;

