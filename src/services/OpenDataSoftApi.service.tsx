import HttpClientStore from "../stores/HttpClient.store";

const urls = {
  jsonPlaceholder: 'https://jsonplaceholder.typicode.com/todos/1',
  leclerc: 'https://www.cartecarburant.leclerc/sibox-api/station?id=69490001&v=3.0'// TODO créer un proxy en php (voir https://github.com/softius/php-cross-domain-proxy)
};

const OpenDataSoftApiService = {

  /**
   * Call the api open data soft and all stations of a specific postal code.
   *
   * @param cp The postal code.
   * @return a promise with the formated json.
   */
   getFuelStatsFromCp : (cp: string) => {

    const facets: string[] = [
      'cp',
      'pop',
      'city',
      'automate_24_24',
      'fuel',
      'shortage',
      'update',
      'services',
      'brand',
      'cp',
      'pop',
      'city',
      'automate_24_24',
      'fuel',
      'shortage',
      'update',
      'services',
      'brand',
    ];

    let url = `/?dataset=prix_des_carburants_j_7&q=${cp}`;
    facets.forEach(facet => url += `&facet=${facet}`);
    return HttpClientStore.OpendDataSoftApi.get(url);
  },

  /**
   * Call a test api (jsonplaceholder.typicode.com) and print the result.
   */
  getSampleDatas : () => {

    fetch(urls.jsonPlaceholder).then((response) => {
      console.log(response);
    });
  }
}

export default OpenDataSoftApiService;