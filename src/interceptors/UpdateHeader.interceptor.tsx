const updateHeaderInterceptor = (axiosInstance: any) => {
  axiosInstance.interceptors.request.use(
    (config: any) => {
      const jwtToken = "Bearer Token from Localstorage";
      config.headers["Authorization"] = jwtToken;
      return config;
    },
    (error: any) => {
        
    }
  );
};
export default updateHeaderInterceptor;