const loadingInterceptor = (axiosInstance: any, store: any) => {
  axiosInstance.interceptors.request.use(
    (config: any) => {
      store.dispatch({ type: 'requestStart' });
      return config;
    },
    (error: any) => {
      store.dispatch({ type: 'requestEnd' });
    }
  );
  axiosInstance.interceptors.response.use(
    (config: any) => {
      store.dispatch({ type: 'requestEnd' });
      return config;
    },
    (error: any) => {
      store.dispatch({ type: 'requestEnd' });
    }
  );
};
export default loadingInterceptor;