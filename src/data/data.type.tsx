type Data = {
  address: string,
  date: string,
  brand: string,
  city: string,
  prices: {
    e10: number,
    sp95: number,
    sp98: number,
    gazole: number
  },
  location: {
    x: number,
    y: number
  }
};

export default Data;