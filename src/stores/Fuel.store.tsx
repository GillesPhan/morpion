import { createStore } from "redux";
import Data from "../data/data.type";

const emptyStation: Data = {
  address: '',
  date: '',
  brand: '',
  city: '',
  prices: {
    e10: 0,
    sp95: 0,
    sp98: 0,
    gazole: 0
  },
  location: { x: 0, y:0 }
};
const fuelReducer = (state = {selectedStation: emptyStation, selectedCp: "", responseData: {}}, action: any) => {

  switch (action.type) {
    case 'setSelectedStation':
      return {
        selectedStation: action.value,
        selectedCp: state.selectedCp,
        responseData: state.responseData 
      };
    case 'setSelectedCp':
      return {
        selectedStation: state.selectedStation,
        selectedCp: action.value,
        responseData: state.responseData
      };
    case 'setResponseData':
      return {
        selectedStation: state.selectedStation,
        selectedCp: state.selectedCp,
        responseData: action.value
      };
    default:
      return state;
  }
};

const store = createStore(fuelReducer);

const FuelStore = {
  getStore: () => store
};

export default FuelStore;