import { createStore } from "redux";

// TODO this service should be deleted!
const counterReducer = (state = { value: 0 }, action: any) => {

  switch (action.type) {
    case 'counter/incremented':
      return { value: state.value + 1 }
    case 'counter/decremented':
      return { value: state.value - 1 }
    default:
      return state
  }
}
const store = createStore(counterReducer);

const TestStore = {
  getStore: () => store,
}

// const store = TestStore.initStore();

// store.subscribe(() => console.log(store.getState));

// store.subscribe(() => console.log(store.getState));
// store.dispatch({ type: 'counter/incremented' });
// store.dispatch({ type: 'counter/decremented' });


export default TestStore;