import axios from "axios";
import { createStore } from "redux";
import loadingInterceptor from "../interceptors/Loading.interceptor";
import updateHeaderInterceptor from "../interceptors/UpdateHeader.interceptor";


const loadingReducer = (state = { isRequestPending: false }, action: any) => {

  switch (action.type) {
    case 'requestStart':
      return { isRequestPending: true }
    case 'requestEnd':
      return { isRequestPending: false }
    default:
      return state
  }
}
const store = createStore(loadingReducer);

const HttpClientStore = {
  OpendDataSoftApi: axios.create({
    baseURL: process.env.REACT_APP_OPENDATASOFT_API_URL,
  }),
  LocalhostApi: axios.create({
    baseURL: process.env.REACT_APP_BACKEND_API_URL, // Currently, this is not used!
  }),
  getStore: () => store,
};

// intercept urls for Bearer Token
updateHeaderInterceptor(HttpClientStore.LocalhostApi);

// intercept urls for loading
loadingInterceptor(HttpClientStore.OpendDataSoftApi, store);
loadingInterceptor(HttpClientStore.LocalhostApi, store);

export default HttpClientStore;