import { createStore } from "redux";

// This service is absoulutly stupid, but it is here for test!
const counterReducer = (state = { currentTime: 0 }, action: any) => {

  switch (action.type) {
    case 'updateTime':
      return { currentTime: (new Date()).getTime() };
    default:
      return state;
  }
};

const store = createStore(counterReducer);

setInterval(() => {
  store.dispatch({ type: 'updateTime' });
}, 1000);

const TimeStore = {
  getStore: () => store,
}

export default TimeStore;