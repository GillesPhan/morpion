
# Projet test React

Ce projet n'a que pour but de tester React.

## Pense bête

### `npm start`

Démarre le projet sur le port 3000, voir [http://localhost:3000](http://localhost:3000).


### `npm test`

Lance les TU

### `npm run build`

Build le projet pour la prod, dans le dossier `build`.

### `npm version patch`

Met à jour la version corrective.

## Prochaines étapes

1. ~~Voir les bonnes pratiques pour mettre en place des services~~
2. ~~Mettre en forme (_un minimum_) la page avec du sass et en nettoyant le code de base~~
3. ~~Test des appels avec Axios~~
4. Déplacer le projet dans un autre dépôt (_pour ne pas utiliser le dépôt "morpion"_)
5. ~~Mettre en place les routes~~
6. ~~Un form de recherche basique pour récupérer une autre station (_par code postal_)~~
7. Mettre un backend pour enregistrer l'historique des dernières requêtes
8. Voir quels addons peuvent être installées pour améliorer le confort
9. Mettre des tests cypress
10. ~~Voir la communication parent/enfant~~
11. ~~Mettre en place des shortcut et tester la communication parent/enfants~~
12. ~~Mettre en place un Interceptor pour gérer le loader et disable le bouton 'Search'~~
13. Mettre en forme la partie haute du Home
14. ~~Afficher le numéro de version~~
15. Afficher la date de dernière mise à jour (si possible)
16. ~~Dégager toutes les classes~~
17. Mettre en place un store pour gérer les états de l'app
18. Fixer le responsive
19. ~~Se documenter sur les Hooks ([lien utile](https://www.digitalocean.com/community/tutorials/five-ways-to-convert-react-class-components-to-functional-components-with-react-hooks))~~
20. ~~Utiliser un store (Redux)~~
21. Mapper la structure de retour pour l'appel au service
22. Mise en forme du tableau
23. ~~Loader : Mettre en place un Loader en haut à droite~~
24. Mettre en place du scss
25. Stocker les données en local sur l'appareil
26. Voir comment créer un composant réutilisable
27. Stocker les données en BDD sans login
28. CSS : un :hover sexy
29. Charte de couleurs